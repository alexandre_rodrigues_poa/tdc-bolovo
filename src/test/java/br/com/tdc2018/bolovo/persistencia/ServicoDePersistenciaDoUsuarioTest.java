package br.com.tdc2018.bolovo.persistencia;

import br.com.tdc2018.bolovo.dominio.EMail;
import br.com.tdc2018.bolovo.dominio.Usuario;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.persistence.EntityManager;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

@SpringBootTest
@RunWith(SpringRunner.class)
public class ServicoDePersistenciaDoUsuarioTest {

    @Autowired
    private ServicoDePersistenciaDoUsuario servico;
    @Autowired
    private EntityManager entityManager;

    private final String nome = "Usuário TDC";
    ;

    @Test
    public void testaInserirUsuario() {
        final EMail eMail = new EMail("inserir@tdc.com.br");
        Usuario usuario = servico.criarUsuario(nome, eMail);
        usuario = entityManager.find(Usuario.class, usuario.id());
        assertEquals(nome, usuario.nome());
        assertEquals(eMail, usuario.eMail());
    }

    @Test
    public void testaAlterarDadosDoUsuario() {
        final EMail eMail = new EMail("alterar@tdc.com.br");
        Usuario usuario = servico.criarUsuario(nome, eMail);

        final String novoNome = "Novo nome";
        final EMail novoEmail = new EMail("novoemail@tdc.com.br");
        servico.alterarDadosDoUsuario(usuario.id(), novoNome, novoEmail);

        usuario = entityManager.find(Usuario.class, usuario.id());

        assertEquals(novoNome, usuario.nome());
        assertEquals(novoEmail, usuario.eMail());
    }

    @Test
    public void testaExcluir() {
        final EMail eMail = new EMail("excluir@tdc.com.br");
        Usuario usuario = servico.criarUsuario(nome, eMail);

        servico.excluirUsuario(usuario.id());

        usuario = entityManager.find(Usuario.class, usuario.id());

        assertTrue(usuario.excluido());
    }

    @Test
    public void testaUsarEMailDeUmUsarioExcluido() {
        final EMail eMail = new EMail("usar-excluido@tdc.com.br");
        Usuario usuario = servico.criarUsuario(nome, eMail);

        servico.excluirUsuario(usuario.id());

        servico.criarUsuario(nome, eMail);
    }
}