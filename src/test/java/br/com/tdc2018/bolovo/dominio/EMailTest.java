package br.com.tdc2018.bolovo.dominio;

import org.junit.Test;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.lang.reflect.Field;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class EMailTest {

    @Test
    public void testaToString() {
        final String endereco = "test@tdc.com.br";
        EMail eMail = new EMail(endereco);
        assertEquals(endereco, eMail.toString());
    }

    @Test(expected = IllegalArgumentException.class)
    public void testaEmailInvalido() {
        new EMail("test#tdc.com.br");
    }

    @Test(expected = IllegalArgumentException.class)
    public void testaEmailNaoPodeSerNulo() {
        new EMail("");
    }

    @Test(expected = IllegalArgumentException.class)
    public void testaEmailNaoPodeEstarEmBranco() {
        new EMail(null);
    }

    @Test
    public void testaAnotacaoEmbeddable() {
        Embeddable annotation = EMail.class.getAnnotation(Embeddable.class);
        assertNotNull(annotation);
    }

    @Test
    public void testaAnotacaoColumn() throws NoSuchFieldException {
        Field field = EMail.class.getDeclaredField("endereco");
        Column annotation = field.getAnnotation(Column.class);
        assertNotNull(annotation);
    }
}