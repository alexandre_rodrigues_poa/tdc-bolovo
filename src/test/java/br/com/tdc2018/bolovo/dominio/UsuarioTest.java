package br.com.tdc2018.bolovo.dominio;

import br.com.tdc2018.bolovo.Component;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import javax.persistence.EntityManager;

import java.util.Optional;

import static org.junit.Assert.*;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(PowerMockRunner.class)
@PrepareForTest({Component.class})
public class UsuarioTest {

    private @Mock EntityManager entityManager;
    private @Mock UsuarioRepositorio repositorio;
    private final String nome = "Usuário TDC";
    private final EMail eMail = new EMail("usuario@tdc.com.br");

    @Before
    public void setUp() {
        PowerMockito.mockStatic(Component.class);
        when(Component.get(EntityManager.class)).thenReturn(entityManager);
        when(Component.get(UsuarioRepositorio.class)).thenReturn(repositorio);
        when(repositorio.buscarIdDoUsuarioPorEmail(eMail)).thenReturn(Optional.empty());
    }

    @Test
    public void testaPersistencia() {
        Usuario usuario = new Usuario(nome, eMail);
        verify(entityManager, times(1)).persist(usuario);
    }

    @Test
    public void testaObterNome() {
        Usuario usuario = new Usuario(nome, eMail);
        assertEquals(nome, usuario.nome());
    }

    @Test(expected = IllegalArgumentException.class)
    public void testaNomeEmBranco() {
        new Usuario("", eMail);
    }

    @Test
    public void testaObterEmail() {
        Usuario usuario = new Usuario(nome, eMail);
        assertEquals(eMail, usuario.eMail());
    }

    @Test(expected = IllegalArgumentException.class)
    public void testaEMailNulo() {
        new Usuario(nome, null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testaEMailEmUsoPorOutroUsuario() {
        EMail eMailEmUso = new EMail("emuso@tdc.com.br");
        when(repositorio.buscarIdDoUsuarioPorEmail(eMailEmUso)).thenReturn(Optional.of(45L));
        Usuario usuario = new Usuario(nome, eMailEmUso);
        assertEquals(eMailEmUso, usuario.eMail());
    }

    @Test
    public void destaAlterarDadosDoUsuario() {
        Usuario usuario = new Usuario(nome, eMail);

        final String novoNome = "Novo nome";
        final EMail novoEmail = new EMail("novoemail@tdc.com.br");

        when(repositorio.buscarIdDoUsuarioPorEmail(novoEmail)).thenReturn(Optional.empty());

        usuario.alterar(novoNome, novoEmail);

        assertEquals(novoNome, usuario.nome());
        assertEquals(novoEmail, usuario.eMail());
    }

    @Test(expected = IllegalStateException.class)
    public void destaAlterarDadosDeUmUsuarioExcluido() {
        Usuario usuario = new Usuario(nome, eMail);

        usuario.excluir();
        final String novoNome = "Novo nome";
        final EMail novoEmail = new EMail("novoemail@tdc.com.br");

        when(repositorio.buscarIdDoUsuarioPorEmail(novoEmail)).thenReturn(Optional.empty());

        usuario.alterar(novoNome, novoEmail);
    }
}