package br.com.tdc2018.bolovo.controller;

import br.com.tdc2018.bolovo.dominio.EMail;
import br.com.tdc2018.bolovo.dominio.Usuario;
import br.com.tdc2018.bolovo.dominio.UsuarioRepositorio;
import br.com.tdc2018.bolovo.persistencia.ServicoDePersistenciaDoUsuario;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.embedded.LocalServerPort;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import javax.persistence.EntityManager;

import java.util.List;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toList;
import static org.junit.Assert.*;
import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.HttpStatus.CREATED;
import static org.springframework.http.HttpStatus.OK;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@RunWith(SpringRunner.class)
public class UsuarioControllerTest {

    @LocalServerPort
    private int port;
    @Autowired
    private EntityManager entityManager;
    @Autowired
    private ServicoDePersistenciaDoUsuario servicoDePersistencia;
    @Autowired
    private UsuarioRepositorio usuarioRepositorio;

    private HttpHeaders headers;
    private TestRestTemplate restTemplate;

    @Before
    public void setUp() {
        headers = new HttpHeaders();
        restTemplate = new TestRestTemplate();
    }

    @Test
    public void testaInserirOk() {
        DadosDoUsuario dados = new DadosDoUsuario();
        dados.setNome("Testa Inserir");
        dados.setEmail("inserirok@tdc.com.br");

        HttpEntity<DadosDoUsuario> entity = new HttpEntity<>(dados, headers);
        ResponseEntity<DadosDeRetorno> resposta = restTemplate.exchange(createURLWithPort("/usuario"), HttpMethod.PUT, entity, DadosDeRetorno.class);
        assertEquals(CREATED, resposta.getStatusCode());

        Usuario usuario = entityManager.find(Usuario.class, resposta.getBody().entityId);
        validarDados(dados, usuario);
    }

    @Test
    public void testaInserirComErro() {
        DadosDoUsuario dados = new DadosDoUsuario();
        dados.setEmail("inserirsemnome@tdc.com.br");

        HttpEntity<DadosDoUsuario> entity = new HttpEntity<>(dados, headers);
        ResponseEntity<DadosDeRetorno> resposta = restTemplate.exchange(createURLWithPort("/usuario"), HttpMethod.PUT, entity, DadosDeRetorno.class);
        assertEquals(BAD_REQUEST, resposta.getStatusCode());
        assertFalse(resposta.getBody().mensagensDeErro.isEmpty());
    }

    @Test
    public void testaEditarOk() {
        Usuario usuario = servicoDePersistencia.criarUsuario("Nome Edicao", new EMail("emailedicao@tdc.com.br"));

        long id = usuario.id();

        DadosDoUsuario dados = new DadosDoUsuario();
        dados.setId(id);
        dados.setNome("Novo Nome");
        dados.setEmail("novoemailedicao@tdc.com.br");

        HttpEntity<DadosDoUsuario> entity = new HttpEntity<>(dados, headers);
        ResponseEntity<DadosDeRetorno> response = restTemplate.exchange(createURLWithPort("/usuario"), HttpMethod.POST, entity, DadosDeRetorno.class);
        assertEquals(OK, response.getStatusCode());

        usuario = entityManager.find(Usuario.class, id);
        validarDados(dados, usuario);
    }

    @Test
    public void testaEditarComErro() {
        Usuario usuario = servicoDePersistencia.criarUsuario("Nome Edicao", new EMail("emailedicaocomerro@tdc.com.br"));

        long id = usuario.id();

        DadosDoUsuario dados = new DadosDoUsuario();
        dados.setId(id);
        dados.setEmail("abc");

        HttpEntity<DadosDoUsuario> entity = new HttpEntity<>(dados, headers);
        ResponseEntity<DadosDeRetorno> response = restTemplate.exchange(createURLWithPort("/usuario"), HttpMethod.POST, entity, DadosDeRetorno.class);
        assertEquals(BAD_REQUEST, response.getStatusCode());
        assertFalse(response.getBody().mensagensDeErro.isEmpty());
    }

    @Test
    public void testaExcluir() {
        Usuario usuario = servicoDePersistencia.criarUsuario("Nome Exclusao", new EMail("emailexclusao@tdc.com.br"));
        ResponseEntity<DadosDeRetorno> resposta = restTemplate.exchange(createURLWithPort("/usuario/" + usuario.id()), HttpMethod.DELETE, null, DadosDeRetorno.class);

        assertEquals(OK, resposta.getStatusCode());
        assertEquals(usuario.id(), resposta.getBody().entityId);
    }

    @Test
    public void testaBuscar() {
        Usuario usuario = servicoDePersistencia.criarUsuario("Nome Busca", new EMail("emailbusca@tdc.com.br"));
        ResponseEntity<DadosDoUsuario> resposta = restTemplate.getForEntity (createURLWithPort("/usuario/" + usuario.id()), DadosDoUsuario.class);
        assertEquals(OK, resposta.getStatusCode());
        assertEquals(usuario.id(), resposta.getBody().getId());
    }

    @Test
    public void testaListar() throws JsonProcessingException {
        servicoDePersistencia.criarUsuario("Nome Listar 01", new EMail("emaillista01@tdc.com.br"));
        servicoDePersistencia.criarUsuario("Nome Listar 01", new EMail("emaillista02@tdc.com.br"));

        Stream<Usuario> usuarios = usuarioRepositorio.listar().stream();
        List<DadosDoUsuario> dados = usuarios.map(DadosDoUsuario::criarComBaseNo).collect(toList());

        ObjectMapper mapper = new ObjectMapper();
        String repostaExperada = mapper.writeValueAsString(dados);

        ResponseEntity<String> resposta = restTemplate.getForEntity(createURLWithPort("/usuario/listar"), String.class);
        assertEquals(OK, resposta.getStatusCode());
        assertEquals(repostaExperada, resposta.getBody());

    }

    private String createURLWithPort(String uri) {
        return "http://localhost:" + port + uri;
    }

    private void validarDados(DadosDoUsuario dadosDoCliente, Usuario usuario) {
        assertEquals(dadosDoCliente.getNome(), usuario.nome());
        assertEquals(new EMail(dadosDoCliente.getEmail()), usuario.eMail());
    }
}