package br.com.tdc2018.bolovo.controller;

import br.com.tdc2018.bolovo.dominio.EMail;
import br.com.tdc2018.bolovo.dominio.Usuario;
import br.com.tdc2018.bolovo.dominio.UsuarioRepositorio;
import br.com.tdc2018.bolovo.persistencia.ServicoDePersistenciaDoUsuario;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.Errors;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toList;

@RestController
@RequestMapping("/usuario")
public class UsuarioController {

    @Autowired
    private UsuarioRepositorio repositorio;
    @Autowired
    private ServicoDePersistenciaDoUsuario servico;

    @GetMapping(path="/{id}", produces="application/json; charset=UTF-8")
    public ResponseEntity<DadosDoUsuario> buscarUsuario(@PathVariable("id") long id) {
        Optional<Usuario> usuario = repositorio.buscarPeloId(id);

        if (usuario.isPresent()) {
            DadosDoUsuario dados = DadosDoUsuario.criarComBaseNo(usuario.get());
            return ResponseEntity.ok(dados);
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @GetMapping(path="/listar", produces="application/json; charset=UTF-8")
    public List<DadosDoUsuario> listar() {
        Stream<Usuario> clientes = repositorio.listar().stream();
        return clientes.map(DadosDoUsuario::criarComBaseNo).collect(toList());
    }

    @PutMapping(produces="application/json; charset=UTF-8")
    public ResponseEntity<DadosDeRetorno> inserir(@Validated @RequestBody DadosDoUsuario dados, Errors errors) {
        if (errors.hasErrors()) {
            DadosDeRetorno retorno = DadosDeRetorno.criarCom(dados.getId(), errors.getFieldErrors());
            return ResponseEntity.badRequest().body(retorno);
        }

        Usuario usuario = servico.criarUsuario(dados.getNome(), new EMail(dados.getEmail()));

        DadosDeRetorno retorno = DadosDeRetorno.criarCom(usuario.id());
        URI uri =  URI.create("/cliente/" + dados.getId());
        return ResponseEntity.created(uri).body(retorno);
    }

    @PostMapping(produces="application/json; charset=UTF-8")
    public ResponseEntity<DadosDeRetorno> editar(@Validated @RequestBody DadosDoUsuario dados, Errors errors) {
        if (errors.hasErrors()) {
            DadosDeRetorno retorno = DadosDeRetorno.criarCom(dados.getId(), errors.getFieldErrors());
            return ResponseEntity.badRequest().body(retorno);
        }

        servico.alterarDadosDoUsuario(dados.getId(), dados.getNome(), new EMail(dados.getEmail()));

        DadosDeRetorno retorno = DadosDeRetorno.criarCom(dados.getId());

        return ResponseEntity.ok(retorno);
    }

    @DeleteMapping(path="/{id}", produces="application/json; charset=UTF-8")
    public ResponseEntity<DadosDeRetorno> excluir(@PathVariable("id") long id) {
        return servico.excluirUsuario(id) ? ResponseEntity.ok(DadosDeRetorno.criarCom(id)) : ResponseEntity.notFound().build();
    }

}
