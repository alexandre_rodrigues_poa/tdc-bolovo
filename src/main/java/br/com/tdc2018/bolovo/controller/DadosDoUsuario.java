package br.com.tdc2018.bolovo.controller;

import br.com.tdc2018.bolovo.dominio.Usuario;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.NotNull;

public class DadosDoUsuario {

    private long id;
    @NotNull @NotBlank
    private String nome;
    @Email
    private String email;

    public DadosDoUsuario() {}


    public static DadosDoUsuario criarComBaseNo(Usuario usuario) {
        DadosDoUsuario dadosDoUsuario = new DadosDoUsuario();
        dadosDoUsuario.id = usuario.id();
        dadosDoUsuario.nome = usuario.nome();
        dadosDoUsuario.email = usuario.eMail().toString();
        return dadosDoUsuario;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public String toString() {
        return "DadosDoUsuario{" + "id=" + id +
                ", nome='" + nome + '\'' +
                ", email='" + email +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        DadosDoUsuario that = (DadosDoUsuario) o;

        if (id != that.id) return false;
        if (nome != null ? !nome.equals(that.nome) : that.nome != null) return false;
        return email != null ? email.equals(that.email) : that.email == null;
    }

    @Override
    public int hashCode() {
        int result = (int) (id ^ (id >>> 32));
        result = 31 * result + (nome != null ? nome.hashCode() : 0);
        result = 31 * result + (email != null ? email.hashCode() : 0);
        return result;
    }

}
