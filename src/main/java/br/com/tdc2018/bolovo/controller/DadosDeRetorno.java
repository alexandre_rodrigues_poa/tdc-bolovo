package br.com.tdc2018.bolovo.controller;

import org.springframework.validation.FieldError;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toList;

@SuppressWarnings("WeakerAccess")
public class DadosDeRetorno {
    public long entityId = 0;
    public final List<String> mensagensDeErro = new ArrayList<>();

    public static DadosDeRetorno criarCom(long entityId, List<FieldError> fieldErrors) {
        Stream<FieldError> errors = fieldErrors.stream();

        Stream<String> mensagens = errors.
                map(error -> String.format("Campo '%s' inválido. Messagem De Erro: %s. Valor informado: %s",
                        error.getField(), error.getDefaultMessage(), error.getRejectedValue()));

        DadosDeRetorno dados = new DadosDeRetorno();
        dados.entityId = entityId;
        dados.mensagensDeErro.addAll(mensagens.collect(toList()));

        return dados;
    }

    public static DadosDeRetorno criarCom(long entityId) {
        DadosDeRetorno dados = new DadosDeRetorno();
        dados.entityId = entityId;
        return dados;
    }

    public static DadosDeRetorno criarCom(long entityId, String mensagemDeErro) {
        DadosDeRetorno dados = new DadosDeRetorno();
        dados.entityId = entityId;
        dados.mensagensDeErro.add(mensagemDeErro);
        return dados;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("DadosDeRetorno{");
        sb.append("entityId=").append(entityId);
        sb.append(", mensagensDeErro=").append(mensagensDeErro);
        sb.append('}');
        return sb.toString();
    }
}
