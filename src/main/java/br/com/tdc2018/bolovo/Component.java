package br.com.tdc2018.bolovo;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.annotation.Scope;

@org.springframework.stereotype.Component
@Scope("singleton")
public class Component implements ApplicationContextAware {

	private static ApplicationContext context;
	
	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		context = applicationContext;
	}

	public static <T> T get(Class<T> requiredType) {
		return context.getBean(requiredType);
	}

}
