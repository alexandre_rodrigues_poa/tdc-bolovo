package br.com.tdc2018.bolovo.persistencia;

import br.com.tdc2018.bolovo.dominio.EMail;
import br.com.tdc2018.bolovo.dominio.Usuario;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.Optional;

@Service
public class ServicoDePersistenciaDoUsuario {

    @Autowired
    private EntityManager entityManager;

    @Transactional
    public Usuario criarUsuario(String nome, EMail eMail) {
        return new Usuario(nome, eMail);
    }

    @Transactional
    public void alterarDadosDoUsuario(long id, String nome, EMail eMail) {
        Usuario usuario = entityManager.find(Usuario.class, id);
        usuario.alterar(nome, eMail);
    }

    @Transactional
    public boolean excluirUsuario(long id) {
        Usuario usuario = entityManager.find(Usuario.class, id);
        if (usuario != null) {
            usuario.excluir();
            return true;
        }
        return false;
    }
}
