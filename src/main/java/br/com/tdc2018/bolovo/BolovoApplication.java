package br.com.tdc2018.bolovo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BolovoApplication {

	public static void main(String[] args) {
		SpringApplication.run(BolovoApplication.class, args);
	}
}
