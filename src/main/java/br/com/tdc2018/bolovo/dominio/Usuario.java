package br.com.tdc2018.bolovo.dominio;

import br.com.tdc2018.bolovo.Component;
import org.apache.commons.lang3.StringUtils;

import javax.persistence.*;
import java.util.Optional;

import static javax.persistence.GenerationType.AUTO;

@Entity
public class Usuario {

    @Id @GeneratedValue(strategy=AUTO)
    private long id;
    @Column
    private String nome;
    @Embedded
    private EMail eMail;
    @Column
    private boolean excluido;

    private Usuario() {};

    public Usuario(String nome, EMail eMail) {
        definirDadosDoUsuario(nome, eMail);
        EntityManager entityManager = Component.get(EntityManager.class);
        entityManager.persist(this);
    }

    public long id() {
        return id;
    }

    public String nome() {
        return nome;
    }

    public EMail eMail() {
        return eMail;
    }

    public boolean excluido() {
        return excluido;
    }

    public void excluir() {
        excluido = true;
    }

    private void definirDadosDoUsuario(String nome, EMail eMail) {
        if (StringUtils.isBlank(nome))
            throw new IllegalArgumentException("O nome não pode estar em branco");
        if (eMail == null)
            throw new IllegalArgumentException("O eMail não pode ser nulo");
        if (emailEmUsoPorOutroUsuario(eMail))
            throw new IllegalArgumentException(String.format("O e-mail '%s' já está em uso por outro usuário", eMail));
        this.nome = nome;
        this.eMail = eMail;
    }

    private boolean emailEmUsoPorOutroUsuario(EMail eMail) {
        UsuarioRepositorio repositorio = Component.get(UsuarioRepositorio.class);
        Optional<Long> idDoUsuarioDoEmail = repositorio.buscarIdDoUsuarioPorEmail(eMail);
        return idDoUsuarioDoEmail.filter(id -> id != this.id).isPresent();
    }

    public void alterar(String nome, EMail eMail) {
        if (excluido)
            throw new IllegalStateException("Não é possivel alterar os dados de um usuário excluido");

        definirDadosDoUsuario(nome, eMail);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Usuario usuario = (Usuario) o;
        return id == usuario.id;
    }

    @Override
    public int hashCode() {
        return (int) (id ^ (id >>> 32));
    }

    @Override
    public String toString() {
        return "Usuario{" + "id=" + id +
                ", nome='" + nome + '\'' +
                ", eMail=" + eMail +
                ", excluido=" + excluido +
                '}';
    }
}
