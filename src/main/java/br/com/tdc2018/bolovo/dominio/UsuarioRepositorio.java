package br.com.tdc2018.bolovo.dominio;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;
import java.util.List;
import java.util.Optional;

@Repository
public class UsuarioRepositorio {
    @Autowired
    private EntityManager entityManager;

    public Optional<Long> buscarIdDoUsuarioPorEmail(EMail eMail) {
        String jpql = "SELECT user.id FROM Usuario user WHERE user.excluido = false AND user.eMail = :eMail" ;

        TypedQuery<Long> query = entityManager.createQuery(jpql, Long.class);
        query.setParameter("eMail", eMail);

        try {
            return Optional.of(query.getSingleResult());
        } catch (NoResultException e) {
            return Optional.empty();
        }
    }

    public Optional<Usuario> buscarPeloId(long id) {
        String jpql = "SELECT user FROM Usuario user WHERE user.excluido = false AND user.id= :id" ;

        TypedQuery<Usuario> query = entityManager.createQuery(jpql, Usuario.class);
        query.setParameter("id", id);

        try {
            return Optional.of(query.getSingleResult());
        } catch (NoResultException e) {
            return Optional.empty();
        }
    }

    public List<Usuario> listar() {
        String jpql = "SELECT user FROM Usuario user WHERE user.excluido = false" ;
        TypedQuery<Usuario> query = entityManager.createQuery(jpql, Usuario.class);
        return query.getResultList();
    }
}
