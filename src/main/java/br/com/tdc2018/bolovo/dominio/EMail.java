package br.com.tdc2018.bolovo.dominio;

import org.apache.commons.validator.routines.EmailValidator;
import org.hibernate.validator.constraints.Email;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class EMail {

    @Column
    private String endereco;

    private EMail(){};

    public EMail(String endereco) {
        EmailValidator validator = EmailValidator.getInstance();
        if (!validator.isValid(endereco))
            throw new IllegalArgumentException(String.format("O endereço de e-mail '%s' informado é inválido", endereco));
        this.endereco = endereco;
    }

    @Override
    public String toString() {
        return endereco;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        EMail eMail = (EMail) o;
        return endereco.equals(eMail.endereco);
    }

    @Override
    public int hashCode() {
        return endereco.hashCode();
    }
}
